package ro.orangetraining;


import java.util.*;

public class Main {

    public static void main(String[] args) {
        
        HashSet<String> myEmployeesSet1 = new HashSet<>();
        myEmployeesSet1.add("Samuel Smith");
        myEmployeesSet1.add("Bill Williams");
        myEmployeesSet1.add("Anthony Johnson");
        myEmployeesSet1.add("Cartner Caragher");
        System.out.println("myEmployeesSet1: " +myEmployeesSet1);

        List list=new ArrayList();
        list.add("Astrid Conner");
        list.add("Christopher Adams");
        list.add("Antoine Griezmann");
        list.add("Adam Sandler");
        list.add("Bailey Aidan");
        list.add("Carl Edwin");
        list.add("Carl Edwin");
        System.out.println("list: " +list);

        HashSet myEmployeesSet2=new HashSet(list);
        System.out.println("myEmployeesSet2: " +myEmployeesSet2);



        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " +myEmployeesSet1.equals(myEmployeesSet2));

        myEmployeesSet2.remove("Adam Sandler");
        System.out.println("myEmployeesSet2: " +myEmployeesSet2);
        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " +myEmployeesSet1.equals(myEmployeesSet2));
        System.out.println("myEmployeesSet1 contains all the elements: " +myEmployeesSet1.containsAll(list));
        System.out.println("myEmployeesSet2 contains all the elements: " +myEmployeesSet2.containsAll(list));


        //Iterator
        Iterator it =myEmployeesSet1.iterator();
        while(it.hasNext()) {
            Object next = it.next();
            System.out.println("Iterator loop: " +next);
        }


        myEmployeesSet1.clear();
        System.out.println("myEmployeesSet1 is Empty: " +myEmployeesSet1.isEmpty());
        System.out.println("myEmployeesSet1 has: " +myEmployeesSet1.size() +" Elements");
        System.out.println("myEmployeesSet2 has: " +myEmployeesSet2.size() +" Elements");


        String[] theArray=new String[myEmployeesSet2.size()];
        myEmployeesSet2.toArray(theArray);
        System.out.println("The Array:" + Arrays.toString(theArray));
    }
}
